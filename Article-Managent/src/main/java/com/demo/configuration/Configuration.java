package com.demo.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@org.springframework.context.annotation.Configuration
@PropertySource("classpath:ams.properties")
public class Configuration extends WebMvcConfigurerAdapter{
	@Value("${file.upload.client.path}")
	String clientPath;
	@Value("${file.upload.server.path}")
	String serverPath;
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(clientPath+"**").addResourceLocations("file:"+serverPath);
	}
}
