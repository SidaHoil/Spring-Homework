package com.demo.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class Article {
	
	private String createdDate;
	public String thumbnail;
	private Category category;
	private int id;
	@NotBlank
	private String title;
	@NotBlank
	@Size(min=4,max=20)
	private String author;
	@NotBlank
	private String description;
	
	
	public Article(int id, String title, String author,String description, Category category, String createdDate, String thumbnail) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
		this.description = description;
		this.category = category;
		this.createdDate = createdDate;
		this.thumbnail = thumbnail;
	}
	public Article() {
		
	}
	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", author=" + author + ", description=" + description
				+ ", category=" + category + ", createdDate=" + createdDate + ", thumbnail=" + thumbnail + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getthumbnail() {
		return thumbnail;
	}
	public void setthumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	
	

}
