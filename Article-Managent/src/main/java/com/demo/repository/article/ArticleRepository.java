package com.demo.repository.article;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.demo.model.Article;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Update;

@Repository
public interface ArticleRepository {
	@Insert("INSERT INTO tbl_articles (title, description, author, created_date, thumbnail, category_id)  "
			+ "VALUES(#{title}, #{description}, #{author},#{createdDate}, #{thumbnail}, #{category.id})")
	public void add(Article article);

	@Select("SELECT a.id, a.title, a.description, a.author, a.created_date, a.thumbnail, a.category_id, c.name  FROM tbl_articles a INNER JOIN tbl_categories c ON a.category_id=c.id where a.id=#{id}")
	@Results({ 
		@Result(property = "id", column = "id"), 
		@Result(property = "title", column = "title"),
		@Result(property = "description", column = "description"), 
		@Result(property = "author", column = "author"),
		@Result(property = "createdDate", column = "created_date"),
		@Result(property = "thumbnail", column = "thumbnail"),
		@Result(property = "category.id", column = "category_id"),
		@Result(property = "category.name", column = "name") 
	})
	public Article findOne(int id);

	 @Select("SELECT  a.id, a.title, a.description, a.author, a.created_date, a.thumbnail, a.category_id, c.name "
	 + "FROM tbl_articles a INNER JOIN tbl_categories c ON a.category_id=c.id ORDER BY a.id ASC")
	 @Results({ 
			@Result(property = "createdDate", column = "created_date"),
			@Result(property = "category.id", column = "category_id"),
			@Result(property = "category.name", column = "name") 
	})
	public List<Article> findAll();

	@Delete("DELETE FROM tbl_articles WHERE id=#{id}")
	public void delete(int id);
	
	@Select("SELECT a.id, a.title, a.description, a.author, a.created_date, a.thumbnail, a.category_id, c.name  FROM tbl_articles a INNER JOIN tbl_categories c ON a.category_id=c.id where a.id=#{id}")
	@Results({ 
		@Result(property = "id", column = "id"), 
		@Result(property = "title", column = "title"),
		@Result(property = "description", column = "description"), 
		@Result(property = "author", column = "author"),
		@Result(property = "createdDate", column = "created_date"),
		@Result(property = "thumbnail", column = "thumbnail"),
		@Result(property = "category.id", column = "category_id"),
		@Result(property = "category.name", column = "name") 
	})
	public Article view(int id );

	@Update("UPDATE tbl_articles SET title=#{title}, description=#{description}, author=#{author}, category_id=#{category.id} WHERE id=#{id}")
	public void update(Article article);

	public int generateId();
}
