package com.demo.repository.article;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.demo.model.Article;
import com.demo.model.Category;
import com.github.javafaker.Faker;

//@Repository
public class ArticleRepositoryIml implements ArticleRepository{
	
	private List<Article> articles=new ArrayList<>();

	public ArticleRepositoryIml() {
		Faker f=new Faker();
		for(int i=1;i<15;i++) {
			articles.add(new Article(i,f.book().title(),f.artist().name(),f.ancient().hero(),
					new Category(3,"CSS"),new Date().toString(),f.internet().image(100,100,false,null)));
		}
		
	}
	@Override
	public void add(Article article) {
		articles.add(article);
	}

	@Override
	public List<Article> findAll() {
		return articles;
	}

	@Override
	public Article findOne(int id) {
		for(Article article: articles) {
			if(article.getId()==id) {
				return article;
			}
		}
		return null;
	}
	@Override
	public void delete(int id) {
		for(Article article: articles) {
			if(article.getId()==id) {
				articles.remove(article);
				return;
			}
		}
	}
	@Override
	public Article view(int id) {
		return findOne(id);
	}
	@Override
	public void update(Article article) {
		for(int i=0;i<articles.size();i++) {
			if(articles.get(i).getId()==article.getId()) {
				articles.get(i).setTitle(article.getTitle());
				articles.get(i).setDescription(article.getDescription());
				articles.get(i).setCategory(article.getCategory());
				articles.get(i).setAuthor(article.getAuthor());
				return;
			}
		}
		
	}
	@Override
	public int generateId() {
		return articles.lastIndexOf(articles);
	}
	

}
