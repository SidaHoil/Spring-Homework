package com.demo.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.demo.model.Category;
@Repository
public interface CategoryRepository {
	@Select("SELECT id,name FROM tbl_categories ORDER BY id ASC")
	public List<Category> findAll();
	@Select("SELECT id,name FROM tbl_categories WHERE id=#{id}")
	public Category findOne(int id);
	
}
