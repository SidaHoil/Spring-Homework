package com.demo.repository.category;

import static org.hamcrest.CoreMatchers.nullValue;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.demo.model.Category;

@Repository
public class CategoryRepositoryImp implements CategoryRepository{
	private List<Category> categories=new ArrayList<>();
	
	@Override
	public List<Category> findAll() {
		return categories;
	}

	@Override
	public Category findOne(int id) {
		for(Category c:categories) {
			if(c.getId()==id) {
				return c;
			}
		}
		return null;
	}

	public CategoryRepositoryImp() {
		categories.add(new Category(1,"Spring"));
		categories.add(new Category(2,"Java"));
		categories.add(new Category(3,"CSS"));
		categories.add(new Category(4,"HTML"));
		categories.add(new Category(5,"AJAX"));
		categories.add(new Category(6,"JSON"));
		categories.add(new Category(7,"PHP"));
		categories.add(new Category(8,"JavaScript"));
		categories.add(new Category(9,"jQuery"));
		
	}

	

	
}
