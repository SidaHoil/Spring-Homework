
create table tbl_categories(
  id int PRIMARY key auto_increment,
  name varchar(40) not null
);

create table tbl_articles(
  id int primary key auto_increment,
  title varchar(40) not null,
  description text not null ,
  thumbnail varchar not null,
  author varchar(40) not null ,
  created_date varchar(30) not null,
  category_id int REFERENCES tbl_categories (id) on DELETE CASCADE ON UPDATE CASCADE
)
	