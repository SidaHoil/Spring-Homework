
--Insert into table category	
INSERT INTO tbl_categories(name) VALUES('Spring');

INSERT INTO tbl_categories(name) VALUES('Korea');

INSERT INTO tbl_categories(name) VALUES('HTML');

INSERT INTO tbl_categories(name) VALUES('CSS');

INSERT INTO tbl_categories(name) VALUES('Java');


--Insert into talbe article
INSERT INTO tbl_articles (title, description, thumbnail, author, created_date, category_id) VALUES('Spring Boot','is easy to learn','https://t4.ftcdn.net/jpg/01/02/21/15/500_F_102211520_ptjHJyhNvdHL76B4ttZGuzWdtj3RJKmz.jpg','Rod Jonhson','Tue Jun 26 15:56:24 ICT 2018',1);

INSERT INTO tbl_articles (title, description, thumbnail, author, created_date, category_id) VALUES('Korean at HRD','is easy to learn and fun','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROPFHhBUB-g1E01CAx9JQFS3DBXdGGagoYco_kmiXDuBZTuRqb','Sejong the Great','Tue Jun 26 15:56:24 ICT 2018',2);

INSERT INTO tbl_articles (title, description, thumbnail, author, created_date, category_id) VALUES('HTML','is stucture language','http://icons.iconarchive.com/icons/cornmanthe3rd/plex/256/Other-html-5-icon.png','Tim Berners-Lee','Tue Jun 26 15:56:24 ICT 2018',2);

INSERT INTO tbl_articles (title, description, thumbnail, author, created_date, category_id) VALUES('Java','is easy to learn ','https://cdn.iconscout.com/public/images/icon/free/png-256/java-logo-3157e85731f1342a-256x256.png','James Gosling','Tue Jun 26 15:56:24 ICT 2018',5);

INSERT INTO tbl_articles (title, description, thumbnail, author, created_date, category_id) VALUES('CSS','use to apply html element style','https://cdn0.iconfinder.com/data/icons/document-file-types/512/css-512.png','Unkown','Tue Jun 26 15:56:24 ICT 2018',4);
